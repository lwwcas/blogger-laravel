<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.css" type="text/css">
   
    <title>@yield('title')</title>
</head>
<body>
    
  <nav class="navbar navbar-expand-md text-center navbar-light">
    <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse text-center justify-content-start" id="navbar2SupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/logar') }}">Login</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
   
  <div class="bg-primary text-center d-flex align-items-center h-50">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="display-1 text-white">Blogger&nbsp;</h1>
          <p class="lead text-white">Um blogger focado em Laravel&nbsp;</p>
        </div>
      </div>
    </div>
  </div>

  
    <div class="py-5">
    <div class="container">

        @yield('post')
   
    </div>
  </div>
  
    <div class="py-5 section text-center">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="text-primary">Se cadastre em nossa&nbsp; newsletter para ficar por dentro de tudo</h2>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="bg-info py-2 my-4 col-12 align-self-center col-md-12 col-sm-8">
          <form class="form-inline my-3 px-5 bg-info">
            <div class="form-group">
              <input type="email" class="form-control" placeholder="Seu email aqui"> </div>
            <button type="submit" class="btn btn-default">Subscribe</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  
  <div class="bg-primary text-center py-3">
    <div class="container">
      <div class="row">
        <div class="col-4">
          <a><i class="fa fa-5x fa-fw fa-facebook text-white"></i></a>
        </div>
        <div class="col-4">
          <a><i class="fa fa-5x fa-fw fa-twitter text-white"></i></a>
        </div>
        <div class="col-4">
          <a><i class="fa fa-5x fa-fw fa-instagram text-white"></i></a>
        </div>
      </div>
    </div>
  </div>
    
    
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
  <script src="https://pingendo.com/assets/bootstrap/bootstrap-4.0.0-alpha.6.min.js"></script>
</body>
</html>



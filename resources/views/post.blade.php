@extends('base')
 
@section('title', 'Home Page')   
     
@section('post')
     
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-block">
              <h4 class="card-title">Card title</h4>
              <p class="lead text-justify">
              
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquet odio quis condimentum pulvinar. Proin lacinia tellus non augue tincidunt lobortis. Aliquam a lorem vitae erat rutrum auctor. In pharetra mauris quis leo cursus, a tristique purus hendrerit. Maecenas sit amet purus nulla. Nulla ipsum elit, bibendum vel sollicitudin quis, dapibus bibendum leo. Nulla vel tellus eget nunc ultricies semper. Mauris dignissim ante at bibendum blandit. Fusce justo enim, tincidunt quis ultricies sit amet, elementum eu sapien. Curabitur viverra nisi est, nec vestibulum nibh eleifend nec. Integer pharetra, ligula vel laoreet posuere, nunc eros congue nulla, vitae commodo eros quam ac eros.
                
                <br> <br>

                Suspendisse magna massa, viverra sit amet sodales non, scelerisque non ante. Aenean maximus placerat lectus eget ultricies. Maecenas sed accumsan risus, at auctor nisi. Vivamus pulvinar nunc augue, non faucibus mi dignissim nec. Suspendisse magna urna, pellentesque id diam ut, efficitur pulvinar dolor. Integer lobortis nibh ac porttitor semper. Morbi ac ultrices sem. Proin quis accumsan erat. Aliquam porta justo orci, in volutpat turpis laoreet nec. Ut sit amet sem risus. Suspendisse potenti. Nunc aliquet ullamcorper leo sed sagittis. Vestibulum at semper arcu.  
                
              </p>
    
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 text-center">
          <div class="btn-group py-4">
            <a href="#" class="btn btn-outline-primary mx-2 text-right">Gostei&nbsp;</a>
            <a href="#" class="btn btn-outline-primary">Não Gostei</a>
          </div>
        </div>
        <div class="col-md-8   ">
          <div class="blockquote">
            <p>User name</p>
            <div class="blockquote-footer">Tags</div>
          </div>
        </div>
      </div>
        <hr>   
 
@endsection
 

 